#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {
    int stat;
    int rc;

    int forkList[5];
    for (int i = 0; i < 5; i++)
    {
        if ((rc = fork()) == 0){
            
            printf("I am a child. My status is %d\n", i);
            
            exit(i);
            
        }
        sleep(1);
    }
    
    for (int i = 0; i < 5; i++)
    {
        int cpid = waitpid(0, &stat, WUNTRACED);
        if(WIFEXITED(stat)){
            printf("Child %d has been terminated, the status of the child was %d\n", cpid, WEXITSTATUS(stat));
        }
    }
    return 0;
}