#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>




int main() {
    char *s = NULL;
    size_t n = 0, l;

    l = getline(&s, &n, stdin);
    for (int i = 0; i < l; i++)
        s[i] = toupper(s[i]);
    printf("%s", s);
    //you dont free s so we have to free it
    free(s);
    return 0;
}