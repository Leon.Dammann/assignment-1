#include <stdio.h>
#include <stdlib.h>




// the problem is when you call on  a function you store the local varibles in the stack 
// and because the function is recurisve it will use up all the memory in the stack and then it crashes
// to get a higher number you must increase the ulimit with the bash command ulimt -s and set it yo unlimited
int fib(int n, int *dp) {
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;
        
    if (dp[n] < 0)
        dp[n] = (fib(n - 1, dp) + fib(n - 2, dp)) % 10;

    return dp[n];
}

int main() {

    int n;
    if (scanf("%d", &n) < 1)
        return 1;

    int *dp = (int *)malloc(sizeof(int) * (n + 1));
    for (int i = 0; i <= n; i++)
        dp[i] = -1;

    printf("%d\n", fib(n, dp));

    free(dp);

    return 0;
}