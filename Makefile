all: prime swap uppercase fib

clean:
	rm -f prime swap uppercase fib

swap: swap.c
	gcc -g -o swap swap.c

uppercase: uppercase.c
	gcc -g -o uppercase uppercase.c

prime: prime.c
	gcc -g -o prime prime.c

prime: fib.c
	gcc -g -o fib fib.c